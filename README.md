# Nginx
```bash
docker build -t local-nginx:1.0 . 
```
```bash
docker run --rm --name ngxlocal -p 8084:80 local-nginx:1.0
```
```dockerfile
FROM  nginx:1.21-alpine

COPY index.html /usr/share/nginx/html/index.html

EXPOSE 80
```

## React JS
```bash
npx create-react-app react-docker
```
```bash
npm run install
npm start
npm run build
```


```dockerfile
COPY ./build/ /usr/share/nginx/html
```

```bash
docker build -t react-nginx:1.1 -f DockerfileMS .
```



# Mysql + Cliente Web (phpMyadmin)

## Mediante Link v1
```bash
docker run -d --name test-mysql-v1 \
-v mysql-33060:/var/lib/mysql \  
-e MYSQL_ROOT_PASSWORD=test.123 \
-p 33060:3306 \
mysql:5.7.37
```
```bash
docker run --rm --name phpmyadmin-v1 \
-e PMA_ARBITRARY=1 -p 8084:80 \
phpmyadmin
```
```bash
docker run --rm --name phpmyadmin-v1 \
-e PMA_HOST:db -e PMA_PORT:3306 -e PMA_ARBITRARY:1 \
-p 8084:80 \
--link test-mysql-v1:db \
phpmyadmin
```


## crear una red v2
```bash
docker network create basedatos-net
```
```bash
docker run -d --name test-mysql-v2 \
-v mysql-33060:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=test.123 \
-p 33060:3306 --network basedatos-net \
mysql:5.7.37    
```
```bash
docker run --rm --name phpmyadmin-v2 \
-e PMA_HOST=test-mysql-v2 \
-p 8085:80 \
--network basedatos-net \
phpmyadmin   
```
## Docker Compose
```bash
docker-compose up -d
```
```bash
docker-compose down -d
```
![img.png](img%2Fimg.png)



# Practica Docker   

## Crear un Docker Compose para los servicios de Postgres y PGadmin

## Docker Telegram con Python 
 - Dado el siguiente ejemplo:
https://github.com/python-telegram-bot/python-telegram-bot/blob/master/examples/echobot.py


Completar los archivos Dockerfile y docker-compose.yaml para pasar a Docker la implementacion de un echo bot:

### 1 crear la imagen de docker con soporte para el Token como ENV
```
docker build -t telegram-bot .
```

```
docker run --rm -e ........  telegram-bot
```
### 2 crear Docker Compose con parametro del Token
```
docker-compose up
```

### NOTA:
 - se modifico el arcihvo echobot.py para que pueda leer variables de entorno linea 3, 7, 8 y 9
 - Link ejemplo para crear un bot y obtener el token: https://medium.com/geekculture/generate-telegram-token-for-bot-api-d26faf9bf064


# Orquestacion 

## Docker Swarm 

```bash
docker swarm init --advertise-addr <IP>

docker swarm join --token <TOKEN> <IP>:2377

docker node ls
```



### Primer Stack 

[Admin Docker Swarm - Portainer](https://docs.portainer.io/start/install-ce/server/swarm/linux)

```bash
curl -L https://downloads.portainer.io/ce2-17/portainer-agent-stack.yml -o portainer-agent-stack.yml

docker stack deploy -c portainer-agent-stack.yml portainer
```

### Caractiristicas  

#### Health check
El monitoreo de la salud de los contenedores es importante para garantizar su disponibilidad y estabilidad. Docker Swarm ofrece la funcionalidad de "Health check", que permite realizar comprobaciones periódicas del estado del contenedor y su servicio asociado. Si el resultado de la comprobación no es satisfactorio, Swarm lo considera "unhealthy" y puede tomar medidas como reemplazar el contenedor por una nueva instancia.

```yaml
services:
  web:
    image: nginx
    deploy:
      replicas: 3
      healthcheck:
        test: ["CMD", "curl", "-f", "http://localhost"]
        interval: 1m
        timeout: 10s
        retries: 3

```

#### Recursos 
Para garantizar que los contenedores tengan suficientes recursos para su correcto funcionamiento, Docker Swarm permite definir límites de recursos, como la cantidad de memoria o CPU que cada contenedor puede utilizar. Además, también es posible especificar una reserva de recursos, que garantiza que los recursos reservados estén disponibles para el contenedor en todo momento.
```yaml
services:
  db:
    image: mysql
    deploy:
      resources:
        limits:
          cpus: '0.5'
          memory: 1G
        reservations:
          cpus: '0.2'
          memory: 512M


```
#### Replicas
Para garantizar la alta disponibilidad de los servicios, Docker Swarm permite la creación de múltiples réplicas del mismo servicio.

```yaml
services:
  api:
    image: my-api
    deploy:
      replicas: 3
```

#### Placement
Docker Swarm permite especificar dónde se deben ejecutar los contenedores dentro del clúster, lo que puede ayudar a optimizar el rendimiento y la utilización de recursos.
```yaml
services:
  worker:
    image: my-worker
    deploy:
      placement:
        constraints: [node.role == worker, node.labels.type == backend]
```

# DevOPS

## CICD
![img_cicd_setting.png](img%2Fimg_cicd_setting.png)
https://gitlab.com/public-unrn/docker-101/-/settings/ci_cd

### Runner con Docker
- https://docs.gitlab.com/runner/install/docker.html
- https://docs.gitlab.com/runner/register/index.html#docker

#### Gernerar config.toml 

```bash
docker run --rm -v $(pwd)/data:/etc/gitlab-runner gitlab/gitlab-runner:alpine-v15.10.1 register \
--non-interactive \
--executor "docker" \
--docker-image alpine:latest \
--url "https://gitlab.com/" \
--registration-token "<REGISTRATION-TOKEN>" \
--description "docker-runner" \
--maintenance-note "Free-form maintainer notes about this runner" \
--tag-list "docker" \
--run-untagged="true" \
--locked="false" \
--access-level="not_protected"
```

- Copiar el token al archivos config.toml que esta en la carpeta runner.

```toml
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "Gitlab Runner Curso"
  url = "https://gitlab.com/"
  token = "........."
  id = .....
  executor = "docker"
  [runners.custom_build_dir]
  [runners.docker]
    tls_verify = false
    image = "docker:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
```

```bash
docker-compose  up -d
```

- verificar en setting de gitlab que el runner este registrado

Copiar el .gitlab-ci.yml en la raiz del repo para probar que el runner este 
corriendo conectado a Docker

## Sonar

- https://docs.sonarcloud.io/advanced-setup/ci-based-analysis/gitlab-ci/
- https://www.baeldung.com/sonar-qube
- https://www.bitslovers.com/how-to-use-sonarqube-with-docker-and-maven/

### Sonar Local

```bash
npm install -g sonarqube-scanner 
```

```bash
sonar-scanner \                                 
  -Dsonar.projectKey=<NAME> \
  -Dsonar.sources=. \
  -Dsonar.host.url=http://localhost:9001 \
  -Dsonar.login=<TOKEN>
```
